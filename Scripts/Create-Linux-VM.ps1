Param(
  [Parameter(Mandatory=$True)][String]$Name,
  [Int16]$Generation = 1,
  [Int64]$MemoryStartupBytes = 3GB,
  [String]$SwitchName = 'Intel(R) Ethernet Connection I217-LM - Virtual Switch',
  [String]$DiskDirectory = 'D:\vms\hdd',
  [UInt64]$SecondaryDisk = 10GB,
  [String]$MountPoint = '\mnt\disk2',
  [String]$SourceDisk = 'E:\apps\hyper-v_templates\ubuntu-template.vhdx',
  [Parameter(Mandatory=$True)][String]$TemplateUsername = 'username',
  [Parameter(Mandatory=$True)][String]$TemplatePassword = 'password'
)

$ErrorActionPreference = "Stop"

$ModuleDirectoryParent = ( Get-Item $MyInvocation.MyCommand.Definition ).Directory.Parent.FullName

$CustomModule = Import-Module `
    -PassThru `
    -Name "$ModuleDirectoryParent\Build\Custom-Cmdlets.dll"

$existingVM = Get-VM | Where { $_.Name -eq $Name }

if( $existingVM -ne $null ) {
	Write-Host "VM with name '$Name' already exists"
	return 1
}

$switch = Get-VMSwitch | Where { $_.Name -eq $SwitchName }
if( $switch -eq $null ) {
	Write-Host "Switch with name '$SwitchName' doesn't exist"
	return 2
}

$diskPath = Join-Path -Path $DiskDirectory -ChildPath "$name.vhdx"

if( Test-Path $diskPath ) {
	Write-Host "Using existing Hard Disk '$diskPath'"
} else {
	Write-Host "Copying $SourceDisk to $diskPath"
	Copy-Item -Path $SourceDisk -Destination $diskPath
}

# Create VM
New-VM -Name $Name -Generation $Generation -MemoryStartupBytes $MemoryStartupBytes -SwitchName $SwitchName
Write-Host "VM $Name created"

# Remove DVD Drive and SCSI controller
Remove-VMDvdDrive -VMName $Name -ControllerNumber 1 -ControllerLocation 0
Remove-VMScsiController -VMName $Name -ControllerNumber 0

# Add Hard Disk and set vm to boot from it
Add-VMHardDiskDrive -VMName $name -ControllerType IDE  -ControllerNumber 0 -ControllerLocation 0 -Path $diskPath
Set-VMBios -VMName $Name -StartupOrder @( "IDE", "CD", "LegacyNetworkAdapter", "Floppy" )

# Update processor count, disable start action, set stop action to shutdown
Set-VM -Name $Name -ProcessorCount 2 -AutomaticStartAction Nothing -AutomaticStopAction ShutDown

#Start VM
Start-VM -Name $Name

#Get IP
$ip = Start-Job -ScriptBlock {
	Param( $VMName )
	
	$timeout = New-TimeSpan -Seconds 20;
	$adapter = Get-VMNetworkAdapter -VMName $VMName | Select-Object -first 1
	$stopWatch = [diagnostics.stopwatch]::StartNew()
	while( $stopWatch.elapsed -lt $timeout -and $adapter.IPAddresses.count -eq 0 ) {
		Start-Sleep -m 500 #Sleep for half a second
	}
	return @( $null, $adapter.IPAddresses[ 0 ] )[$adapter.IPAddresses.count -gt 1]
} -ArgumentList $Name | Wait-Job | Receive-Job

Write-Host "VM IP: $ip"

Rename-Machine -ComputerIp "$ip" -Username $TemplateUsername -Password $TemplatePassword -NewHostName $Name
Add-NewDisk -ComputerIp "$ip" -Username $TemplateUsername -Password $TemplatePassword -MountLocation "/mnt/disk2"

Remove-Module -Name $CustomModule.Name

#Restart VM
Stop-VM -Name $Name
Start-VM -Name $Name
