﻿using System.Management.Automation;

namespace Custom_Cmdlets.SSH {

	[Cmdlet( VerbsCommon.Add, "NewDisk" )]
	public class AddNewDisk : SSHCmdletBase {

		private string m_deviceLocation = "/dev/sdb";

		[Parameter]
		// ReSharper disable once ConvertToAutoProperty
		public string DeviceLocation {
			get { return m_deviceLocation; }
			set { m_deviceLocation = value; }
		}

		[Parameter( Mandatory = true )]
		public string MountLocation { get; set; }

		protected override void ProcessRecord() {
			/* TODO
				(echo d; echo g; echo n; echo 1; echo ; echo ; echo w) | sudo fdisk /dev/sdb
				mke2fs -F -t ext4 /dev/sdb1
				sudo mkdir -p /mnt/disk2
				sudo mount /dev/sdb /mnt/disk2
			*/

		}
	}
}
