﻿using System.Management.Automation;
using System.Net;

namespace Custom_Cmdlets.SSH {

	public abstract class SSHCmdletBase : Cmdlet {

		[Parameter( Mandatory = true )]
		public IPAddress ComputerIp { get; set; }

		[Parameter( Mandatory = true )]
		public string Username { get; set; }

		[Parameter( Mandatory = true )]
		public string Password { get; set; }

	}
}
