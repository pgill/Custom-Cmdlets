﻿using System;
using System.IO;
using System.Management.Automation;
using System.Net;
using Renci.SshNet;

namespace Custom_Cmdlets.SSH {

	[Cmdlet( VerbsCommon.Rename, "Machine" )]
	public sealed class RenameMachine : SSHCmdletBase {

		private const string CurrentHostName = "cat /etc/hostname";
		private const string Sudo = "echo {0} | sudo -S {1}";
		private const string Reboot = "shutdown -r 0";
		private const string BootstrapFileName = "/bootstrap.sh";
		private const string SheBang = @"#!/bin/bash";
		private const short Permission = 764; // read+write+execute read+write read
		
		private bool m_restartAfterRename;

		[Parameter( Mandatory = true )]
		public string NewHostName { get; set; }

		[Parameter( Mandatory = false )]
		public bool RestartAfterRename {
			get { return m_restartAfterRename; }
			set { m_restartAfterRename = value; }
		}

		protected override void ProcessRecord() {

			if( string.IsNullOrEmpty( NewHostName ) || NewHostName.Length > 15 ) {
				WriteError(
					new ErrorRecord(
						new ArgumentException( "Host name length should be between 1 and 15 inclusive", "NewHostName" ),
						"Invalid Argument",
						ErrorCategory.InvalidArgument,
						NewHostName
					)
				);
				return;
			}

			var info = new ConnectionInfo(
				ComputerIp.ToString(),
				Username,
				new PasswordAuthenticationMethod( Username, Password )
			);

			var client = new SshClient( info );
			try {
				client.Connect();
			} catch( Exception ex ) {
				WriteError(
					new ErrorRecord(
						ex,
						string.Format( "Unable to get connect to {0}", ComputerIp),
						ErrorCategory.WriteError,
						client
					)
				);
				client.Dispose();
				return;
			}
			string currentHostName;
			try {
				 currentHostName = client.RunCommand( CurrentHostName ).Result.Trim().Replace( @"\n", string.Empty );
			} catch( Exception ex ) {
				client.Disconnect();
				client.Dispose();
				WriteError( new ErrorRecord( ex, "Unable to get current hostname", ErrorCategory.WriteError, ComputerIp ) );
				return;
			}

			string newHostName = NewHostName.ToLowerInvariant();
			if( currentHostName != null && currentHostName.ToLowerInvariant() == newHostName ) {
				client.Disconnect();
				client.Dispose();
				WriteProgress(
					new ProgressRecord( 
						1,
						"Hostname lookup",
						string.Format( "Machine name is already {0}", newHostName )
					)
				);

				return;
			}

			var sftpClient = new SftpClient( info );
			try {
				sftpClient.Connect();
			} catch( Exception ex ) {
				WriteError(
					new ErrorRecord(
						ex,
						string.Format( "Unable to get connect to {0} as sftp client", ComputerIp ),
						ErrorCategory.WriteError,
						sftpClient
					)
				);
				client.Disconnect();
				client.Dispose();
				sftpClient.Dispose();
				return;
			}
			string directory = sftpClient.WorkingDirectory;
			string bootstrapScript = directory + BootstrapFileName;

			using( sftpClient ) {
				using( StreamWriter writer = sftpClient.CreateText( bootstrapScript ) ) {
					writer.NewLine = "\n";

					writer.WriteLine( SheBang );
					writer.WriteLine();
					writer.WriteLine( "currentHostname=$(cat /etc/hostname)" );
					writer.WriteLine( "sed -i \"s/$currentHostname/{0}/g\" /etc/hosts", NewHostName );
					writer.WriteLine( "sed -i \"s/$currentHostname/{0}/g\" /etc/hostname", NewHostName );
				}
				sftpClient.ChangePermissions( bootstrapScript, Permission );
				sftpClient.Disconnect();
			}

			using( client ) {
				// Run script as sudo
				client.RunCommand(
					string.Format( Sudo, Password, bootstrapScript )
				);

				// Delete script
				client.RunCommand( string.Format( "rm {0}", bootstrapScript ) );

				if( m_restartAfterRename ) {
					try {
						client.RunCommand( string.Format( Sudo, Password, Reboot ) );
					} catch( Exception ex ) {
						// Disconnect due to restart command can cause execption
						WriteDebug( 
							string.Format(
								"Early disconnect after sending restart request. {0}",
								ex.Message
							)
						);
					}
				}
				client.Disconnect();
			}

		}

	}
}
